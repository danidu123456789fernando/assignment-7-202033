#include <stdio.h>
int main() {
    char str[1000], ch;
    int count = 0;

    printf("ENTER A STRING OR PHRASE: ");
    fgets(str, sizeof(str), stdin);

    printf("ENTER A CHARATER TO FIND ITS FREQUENCY: ");
    scanf("%c", &ch);

    for (int i = 0; str[i] != '\0'; ++i) {
        if (ch == str[i])
            ++count;
    }

    printf("FREQUENCY OF %c = %d", ch, count);
    return 0;
}
